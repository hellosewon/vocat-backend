# -*- coding: utf-8 -*-
import re

names = [
    u'Björn',
    u'Anne-Charlotte',
    u'توماس',
    u'毛',
    u'מיק',
    u'-Björn',
    u'Anne--Charlotte',
    u'Tom_',
    u'홍세원',
    u'홍 세원',
    u'홍 세 원 ',
    u' 홍 세원',
    u'홍-세원',
    u'세-원',
    u'세원-세원',
    u'Fred ',
    u'Sewon Honged ',
    u'Fr ed ',
    u'Fred\n',
    u'Fr  ed',
    ]

for name in names:
    regex = re.compile(r"^[^\W_]+([ \-'‧][^\W_]+)*?\Z", re.U)
    print u'{0:20} {1}'.format(name, regex.match(name) is not None)

