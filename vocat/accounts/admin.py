from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.utils.translation import ugettext_lazy as _

from .models import User, UserProfile, Device, AutoBookCount, AutoSectionCount, AutoVocabularyCount
from .forms import UserChangeForm, UserCreationForm


class UserAdmin(BaseUserAdmin):
    """
    The fields to be used in displaying the User model.
    These override the definitions on the base UserAdmin
    that reference the removed 'username' field
    """
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        (_('Personal info'), {'fields': ('name',)}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2'),
        }),
    )

    # The forms to add and change user instances
    form = UserChangeForm
    add_form = UserCreationForm
    # The fields to be used in displaying the User model.
    list_display = ('email', 'name', 'is_staff')
    search_fields = ('email', 'name')
    ordering = ('email',)


class UserProfileInline(admin.StackedInline):
    model = UserProfile


class UserProfileAdmin(UserAdmin):
    inlines = [UserProfileInline, ]


class DeviceAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'type')


class AutoBookCountAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'count')


class AutoSectionCountAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'count')


class AutoVocabularyCountAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'count')


admin.site.register(User, UserProfileAdmin)
admin.site.register(Device, DeviceAdmin)
admin.site.register(AutoBookCount, AutoBookCountAdmin)
admin.site.register(AutoSectionCount, AutoSectionCountAdmin)
admin.site.register(AutoVocabularyCount, AutoVocabularyCountAdmin)


