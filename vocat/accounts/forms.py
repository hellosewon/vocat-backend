from django.contrib.auth.forms import (
    UserCreationForm as BaseCreationForm,
    UserChangeForm as BaseChangeForm
)
from django import forms

from .models import User, UserProfile


class UserCreationForm(BaseCreationForm):
    """
    A form that creates a user, with no privileges, from the given email and
    password.
    """
    # name = forms.CharField(max_length=70)

    def __init__(self, *args, **kargs):
        super(UserCreationForm, self).__init__(*args, **kargs)

    class Meta:
        model = User
        fields = ('email', 'name',)


class UserChangeForm(BaseChangeForm):
    """A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    password hash display field.
    """
    def __init__(self, *args, **kargs):
        super(UserChangeForm, self).__init__(*args, **kargs)

    class Meta:
        model = User
        fields = '__all__'


class UserProfileChangeForm(forms.ModelForm):

    class Meta:
        model = UserProfile
        exclude = ('user',)
