from django.test import TestCase
from django.core.urlresolvers import reverse, reverse_lazy
from rest_framework import status
from rest_framework.test import APITestCase
from .models import User, UserProfile


class AccountTests(APITestCase):
    def test_registration(self):
        """
        Ensure we can create a new account object.
        """
        url = reverse_lazy('accounts:registration')
        data = {
            'email': 'test@gmail.com',
            'name': 'Shark',
            'password': 'testpassword',
        }
        # GET Method not allowed for json
        response = self.client.get(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        response = self.client.get(url, data, format='html')
        print response.content
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(User.objects.get().name, 'Shark')
        # self.assertEqual(UserProfile.objects.get().name, 'DabApps')

    def test_registration_done(self):
        pass

    def test_login(self):
        pass

    def test_logout(self):
        pass

    def test_user_profile(self):
        pass

    def test_user_profile_change(self):
        pass

    def test_password_change(self):
        pass

    def test_password_change_done(self):
        pass

    def test_password_reset(self):
        pass

    def test_password_reset_done(self):
        pass

    def test_password_reset_confirm(self):
        pass

    def test_password_reset_complete(self):
        pass