from django.conf.urls import url
from django.views.generic import RedirectView

from .views import *


urlpatterns = [
    # This assumes that Home URL is '/'
    url(r'^$', RedirectView.as_view(url='/'), name='home'),

    # Registration
    url(r'^registration/$', RegistrationView.as_view(), name='registration'),
    url(r'^registration/done/$', RegistrationDoneView.as_view(), name='registration_done'),

    # Login and Logout
    url(r'^login/$', LoginView.as_view(), name='login'),  # Login(HTML POST) or Obtain Token(JSON POST)
    url(r'^logout/$', LogoutView.as_view(), name='logout'),

    # Password change for logged in users
    url(r'^password_change/$', PasswordChangeView.as_view(), name='password_change'),
    url(r'^password_change/done/$', PasswordChangeDoneView.as_view(), name='password_change_done'),

    # Password reset for anonymous users
    url(r'^password_reset/$', PasswordResetView.as_view(), name='password_reset'),
    url(r'^password_reset/done/$', PasswordResetDoneView.as_view(), name='password_reset_done'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    url(r'^reset/done/$', PasswordResetCompleteView.as_view(), name='password_reset_complete'),

    # UserProfile See/Change
    url(r'^profile/$', UserProfileView.as_view(), name='profile_detail'),
    url(r'^profile_change/$', UserProfileChangeView.as_view(), name='profile_change'),
]
