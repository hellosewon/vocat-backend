"""
Code Refactored From django-rest-auth v0.3.4
"""
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse_lazy
from django.contrib.auth import (login as auth_login, logout as auth_logout)
from django.contrib.auth.views import (login, logout,
                                       password_change, password_change_done,
                                       password_reset, password_reset_done,
                                       password_reset_confirm, password_reset_complete)
from django.http import HttpResponseRedirect
from django.shortcuts import resolve_url
from django.template.response import TemplateResponse
from django.utils.translation import ugettext as _

from rest_framework import status
from rest_framework.views import APIView
from rest_framework.generics import GenericAPIView
from rest_framework.authtoken.models import Token
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework.serializers import ValidationError

from .models import UserProfile, Device
from .forms import UserCreationForm, UserProfileChangeForm
from .serializers import (LoginSerializer, TokenSerializer,
                          UserProfileDetailSerializer,
                          PasswordChangeSerializer,
                          PasswordResetSerializer, PasswordResetConfirmSerializer,)

# For def registration()
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.debug import sensitive_post_parameters
from django.contrib.auth.views import deprecate_current_app
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.contrib.sites.shortcuts import get_current_site
from django.utils.http import is_safe_url
import threading


# TODO: Establish ate limiting mechanism. In auth backend maybe? for Login & Registration?

# registration() function implemented separately for no authenticated CSRF protection.
@deprecate_current_app
@sensitive_post_parameters()
@csrf_protect
@never_cache
def registration(request, template_name='accounts/registration_form.html',
                 post_registration_redirect=None,
                 redirect_field_name=REDIRECT_FIELD_NAME,
                 registration_form=UserCreationForm,
                 extra_context=None):
    """
    Displays the registration form and handles the registration action.
    """
    if post_registration_redirect is None:
        post_registration_redirect = reverse_lazy('registration_done')
    else:
        post_registration_redirect = resolve_url(post_registration_redirect)
    redirect_to = request.POST.get(redirect_field_name,
                                   request.GET.get(redirect_field_name, ''))
    if redirect_to == '':
        redirect_to = post_registration_redirect
    print post_registration_redirect, redirect_to
    if request.method == "POST":
        form = registration_form(data=request.POST)
        if form.is_valid():
            # Ensure the user-originating redirection url is safe.
            if not is_safe_url(url=redirect_to, host=request.get_host()):
                redirect_to = resolve_url(settings.LOGIN_REDIRECT_URL)
            # Okay, security check complete. Create an account.
            form.save()
            return HttpResponseRedirect(redirect_to)
    else:
        form = registration_form()
    current_site = get_current_site(request)
    context = {
        'form': form,
        'title': _('Registration'),
        redirect_field_name: redirect_to,
        'site': current_site,
        'site_name': current_site.name,
    }
    if extra_context is not None:
        context.update(extra_context)
    return TemplateResponse(request, template_name, context)


class RegistrationView(GenericAPIView):
    """
    Registers a user account.

    Accept: GET[], POST[email, name, password1, password2]
    GET: If HTML, show registration_page. If JSON, http_method_not_allowed.
    POST: Both HTML and JSON, register_account against .forms.UserCreationForm.
    Return: If HTML, registration_page. If JSON, success/fail message.
    """
    permission_classes = (AllowAny,)

    registration_form = UserCreationForm
    template_name = 'accounts/registration_form.html'
    post_registration_redirect = reverse_lazy('accounts:registration_done')

    def get(self, request):
        # If registration from HTML, show_registration_page
        if request.accepted_renderer.format == 'html':
            return self.show_registration_page()
        # If registration from JSON, deny.
        return self.http_method_not_allowed(request)

    def post(self, request):
        # If registration from HTML, register_from_html
        if request.accepted_renderer.format == 'html':
            return self.register_from_html()
        # If registration from JSON, register_from_json
        return self.register_from_json()

    def show_registration_page(self):
        if self.request.user.is_authenticated():
            return HttpResponseRedirect(resolve_url(settings.LOGIN_REDIRECT_URL))
        self.request._request.GET = self.request.GET
        return registration(self.request._request,
                            post_registration_redirect=self.post_registration_redirect)

    def register_from_html(self):
        self.request._request.POST = self.request.POST
        return registration(self.request._request,
                            post_registration_redirect=self.post_registration_redirect)  # Redirect? Get?

    def register_from_json(self):
        data = self.request.data
        response = Response({"success": "New account has been created."}, status=status.HTTP_200_OK)
        return self.save_registration(data, response)

    def retrieve_form(self, data):
        form = self.registration_form(data)
        return form

    def return_form(self, form):  # HTML error Return Function
        context = {
            'form': form,
            'title': _('Registration'),
        }
        return TemplateResponse(self.request, self.template_name, context)

    def save_registration(self, data, response):  # Save, JSON error Return Function
        form = self.retrieve_form(data)
        if form.is_valid():
            form.save()
            return response
        if self.request.accepted_renderer.format == 'html':
            return self.return_form(form)
        raise ValidationError(form.errors)


class RegistrationDoneView(GenericAPIView):
    """
    Shows the registration_done_page only for HTML.

    Accept: GET[]
    GET: If HTML, show registration_done_page. If JSON, http_method_not_allowed.
    Return: If HTML, registration_done_page.
    """
    permission_classes = (AllowAny,)

    template_name = 'accounts/registration_done.html'

    def get(self, request):
        # If registration_done from HTML, show_registration_done_page
        if request.accepted_renderer.format == 'html':
            return self.show_registration_done_page()
        # If registration_done from JSON, deny.
        return self.http_method_not_allowed(request)

    def show_registration_done_page(self):
        context = {
            'title': _('Registration successful'),
        }
        return TemplateResponse(self.request, self.template_name, context)


class LoginView(GenericAPIView):
    """
    Logs in the user by checking the credentials.
    Calls Django Auth login method to register User ID in Django session framework

    Accept: GET[], POST[username, password]
    GET: If HTML, show login page. If JSON, http_method_not_allowed.
    POST: If HTML, login against django.contrib.auth.views.
        If JSON, login against LoginSerializer.
    Return: If HTML, login page. If JSON, REST Framework Token key.
    """
    permission_classes = (AllowAny,)
    serializer_class = LoginSerializer
    token_model = Token
    response_serializer = TokenSerializer

    # Parameters for django.contrib.auth.views
    template_name = 'accounts/login.html'
    # redirect_field_name=REDIRECT_FIELD_NAME,
    # authentication_form=AuthenticationForm,
    # extra_context=None

    def get(self, request):
        # If GET request from Web HTML
        if request.accepted_renderer.format == 'html':
            return self.show_login_page()
        # If GET request from json Do not allow by checking authentication?
        return self.http_method_not_allowed(request)

    def post(self, request):
        # If login from HTML, login_from_html
        if request.accepted_renderer.format == 'html':
            return self.login_from_html()
        # If login from JSON, login_from_json
        return self.login_from_json()

    def show_login_page(self):
        # If user is already logged in, redirect to LOGIN_REDIRECT_URL
        if self.request.user.is_authenticated():
            return HttpResponseRedirect(resolve_url(settings.LOGIN_REDIRECT_URL))
        return self.login_from_html()

    def login_from_html(self):
        # Copying all wsgihttprequest's POST data to httprequest including 'csrfmiddlewaretoken'
        # self.request.COOKIES['csrftoken']?
        self.request._request.POST = self.request.POST
        return login(self.request._request, template_name=self.template_name)

    def login_from_json(self):
        serializer = self.get_serializer(data=self.request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = self.token_model.objects.get_or_create(user=user)
        device_type = self.request.data['type'] or 0
        device_id = Device.objects.create(user=user, type=device_type).id
        return Response({'token': self.response_serializer(token).data['key'], 'device_id': device_id}, status=status.HTTP_200_OK)


class LogoutView(APIView):
    """
    Logs out the user.
    Calls Django logout method or delete the token object assigned to the current User object.

    Accept: GET[]
    GET: If HTML, logout against django.contrib.auth.views.
        If JSON, delete the Token object.
    Return: If HTML, login_page. If JSON, rest_framework_token_key.
    """
    permission_classes = (AllowAny,)

    # Parameters for django.contrib.auth.views
    # next_page=None,
    template_name = 'accounts/logged_out.html'
    # redirect_field_name=REDIRECT_FIELD_NAME,
    # extra_context=None

    def get(self, request):
        # If logout from HTML, logout_from_html
        if request.accepted_renderer.format == 'html':
            return self.logout_from_html()
        # If logout from JSON, logout_from_json
        return self.logout_from_json()

    def logout_from_html(self):
        if not self.request.user.is_authenticated():
            return HttpResponseRedirect(resolve_url(settings.LOGIN_REDIRECT_URL))
        self.request._request.GET = self.request.GET
        return logout(self.request._request, template_name=self.template_name)

    def logout_from_json(self):
        try:
            self.request.user.auth_token.delete()
        except (AttributeError, ObjectDoesNotExist):
            pass
        auth_logout(self.request)
        return Response({"success": "Successfully logged out."}, status=status.HTTP_200_OK)


class UserProfileView(GenericAPIView):
    """
    Returns the user profile detail of the currently authenticated user.

    Accept: GET[]
    GET: If HTML, show_profile_page..
        If JSON, retrieve_from_json.
    Return: If HTML, profile_page. If JSON, profile_json_object.
    """
    permission_classes = (IsAuthenticated,)
    serializer_class = UserProfileDetailSerializer

    template_name = 'accounts/profile_detail.html'

    def get(self, request):
        profile = self.retrieve_profile()
        serializer = self.get_serializer(profile)
        data = serializer.data
        # If profile from HTML, show_profile_page
        if request.accepted_renderer.format == 'html':
            return self.show_profile_page(data)
        # If profile from JSON, retrieve_profile
        return self.retrieve_from_json(data)

    def retrieve_profile(self):
        try:
            profile = UserProfile.objects.get(user=self.request.user)
        except ObjectDoesNotExist:
            return None
        return profile

    def show_profile_page(self, data):
        context = {
            'title': _('Profile'),
            'data': data,
        }
        return TemplateResponse(self.request, self.template_name, context)

    @staticmethod
    def retrieve_from_json(data):
        # from rest_framework.renderers import JSONRenderer
        # json = JSONRenderer().render(data)
        return Response(data, status=status.HTTP_200_OK)


class UserProfileChangeView(GenericAPIView):
    """
    Changes the user profile of the currently authenticated user.

    Accept: GET[], POST[__all__]
    GET: If HTML, show profile_change_page. If JSON, http_method_not_allowed.
    POST: Both HTML and JSON, change_profile against .forms.UserProfileChangeForm.
    Return: If HTML, profile_change_page. If JSON, success/fail message.
    """
    permission_classes = (IsAuthenticated,)

    profile_change_form = UserProfileChangeForm
    template_name = 'accounts/profile_change.html'
    post_change_redirect = reverse_lazy('accounts:profile_detail')

    def get(self, request):
        # If profile from HTML, show_profile_change_page
        if request.accepted_renderer.format == 'html':
            return self.show_profile_change_page()
        # If profile from JSON, deny.
        return self.http_method_not_allowed(request)

    def post(self, request):
        # If profile from HTML, change_profile_from_html
        if request.accepted_renderer.format == 'html':
            return self.change_profile_from_html()
        # If profile from JSON, change_profile_from_json
        return self.change_profile_from_json()

    def show_profile_change_page(self):
        profile = self.retrieve_profile()
        form = self.retrieve_form(None, profile)
        return self.return_form(form)

    def change_profile_from_html(self):
        data = self.request.POST
        response = HttpResponseRedirect(self.post_change_redirect)
        return self.save_changed_profile(data, response)

    def change_profile_from_json(self):
        data = self.request.data
        response = Response({"success": "Profile has been changed."}, status=status.HTTP_200_OK)
        return self.save_changed_profile(data, response)

    def save_changed_profile(self, data, response):  # Save, JSON error Return Function
        profile = self.retrieve_profile()
        form = self.retrieve_form(data, profile)
        if form.is_valid():
            profile.save()
            return response
        if self.request.accepted_renderer.format == 'html':
            return self.return_form(form)
        raise ValidationError(form.errors)

    def retrieve_profile(self):
        try:
            profile = UserProfile.objects.get(user=self.request.user)
        except ObjectDoesNotExist:
            return None
        return profile

    def retrieve_form(self, data, profile):
        form = self.profile_change_form(data, instance=profile)
        return form

    def return_form(self, form):  # HTML error Return Function
        context = {
            'form': form,
            'title': _('Profile Change'),
        }
        return TemplateResponse(self.request, self.template_name, context)


class PasswordChangeView(GenericAPIView):
    """
    Changes the password of authenticated user account.

    Accept: GET[], POST[new_password1, new_password2]
    GET: If HTML, show password_change_page. If JSON, http_method_not_allowed.
    POST: If HTML, change_password against django.contrib.auth.views.
        If JSON, change_password against PasswordChangeSerializer.
    Return: If HTML, password_change_page. If JSON, success/fail message.
    """
    permission_classes = (AllowAny,)
    serializer_class = PasswordChangeSerializer

    # Parameters for django.contrib.auth.views
    template_name = 'accounts/password_change_form.html'
    post_change_redirect = reverse_lazy('accounts:password_change_done')
    # password_change_form=PasswordChangeForm,
    # extra_context=None

    def get(self, request):
        # If password_change from HTML, show_password_change_page
        if request.accepted_renderer.format == 'html':
            return self.show_password_change_page()
        # If password_change from JSON, deny.
        return self.http_method_not_allowed(request)

    def post(self, request):
        # If password_change from HTML, change_password_from_html
        if request.accepted_renderer.format == 'html':
            return self.change_password_from_html()
        # If password_change from JSON, change_password_from_json
        if request.user.is_authenticated():
            return self.change_password_from_json()
        return self.http_method_not_allowed(request)

    def show_password_change_page(self):
        self.request._request.GET = self.request.GET
        return password_change(self.request._request, template_name=self.template_name,
                               post_change_redirect=self.post_change_redirect)

    def change_password_from_html(self):
        self.request._request.POST = self.request.POST
        return password_change(self.request._request, template_name=self.template_name,
                               post_change_redirect=self.post_change_redirect)

    def change_password_from_json(self):
        serializer = self.get_serializer(data=self.request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response({"success": "New password has been saved."}, status=status.HTTP_200_OK)


class PasswordChangeDoneView(GenericAPIView):
    """
    Shows the password_change_done_page only for HTML.

    Accept: GET[]
    GET: If HTML, show password_change_done_page. If JSON, http_method_not_allowed.
    Return: If HTML, password_change_done_page.
    """
    permission_classes = (IsAuthenticated,)

    # Parameters for django.contrib.auth.views
    template_name = 'accounts/password_change_done.html'
    # extra_context=None

    def get(self, request):
        # If password_change_done from HTML, show_password_change_done_page
        if request.accepted_renderer.format == 'html':
            return self.show_password_change_done_page()
        # If password_change_done from JSON, deny.
        return self.http_method_not_allowed(request)

    def show_password_change_done_page(self):
        self.request._request.GET = self.request.GET
        return password_change_done(self.request._request, template_name=self.template_name)


class PasswordResetView(GenericAPIView):
    """
    Rests the password of a user account.

    Accept: GET[], POST[email]
    GET: If HTML, show password_reset_page. If JSON, http_method_not_allowed.
    POST: If HTML, reset_password against django.contrib.auth.views.
        If JSON, reset_password against PasswordResetSerializer.
    Return: If HTML, password_reset_page. If JSON, succes/fail message.
    """
    permission_classes = (AllowAny,)
    serializer_class = PasswordResetSerializer

    # TODO: find out what email_template_name and stuff is
    # Parameters for django.contrib.auth.views
    # is_admin_site=False,
    template_name = 'accounts/password_reset_form.html'
    email_template_name = 'accounts/password_reset_email.html'
    # subject_template_name='registration/password_reset_subject.txt',
    # password_reset_form=PasswordResetForm,
    # token_generator=default_token_generator,
    post_reset_redirect = reverse_lazy('accounts:password_reset_done')
    # from_email=None,
    # extra_context=None,
    # html_email_template_name=None,
    # extra_email_context=None

    def get(self, request):
        # If password_reset from HTML, show_password_reset_page
        if request.accepted_renderer.format == 'html':
            return self.show_password_reset_page()
        # If password_reset from JSON, deny.
        return self.http_method_not_allowed(request)

    def post(self, request):
        if request.accepted_renderer.format == 'html':
            return self.reset_password_from_html()
        return self.reset_password_from_json()

    def show_password_reset_page(self):
        self.request._request.GET = self.request.GET
        return password_reset(self.request._request, template_name=self.template_name,
                              email_template_name=self.email_template_name,
                              post_reset_redirect=self.post_reset_redirect)

    def reset_password_from_html(self):
        self.request._request.POST = self.request.POST
        th1 = threading.Thread(name="reset_password_from_html", target=self._reset_password_thread)
        th1.start()
        return HttpResponseRedirect(self.post_reset_redirect)

    def _reset_password_thread(self):
        password_reset(self.request._request, template_name=self.template_name,
                      email_template_name=self.email_template_name,
                      post_reset_redirect=self.post_reset_redirect)

    def reset_password_from_json(self):
        # Create a serializer with request.data
        serializer = self.get_serializer(data=self.request.data, email_template_name=self.email_template_name)
        serializer.is_valid(raise_exception=True)
        th1 = threading.Thread(name="reset_password_from_json", target=serializer.save)
        th1.start()
        # Return the success message with OK HTTP status
        return Response({"success": "Password reset e-mail has been sent."}, status=status.HTTP_200_OK)


class PasswordResetDoneView(GenericAPIView):
    """
    Shows the password_reset_done_page only for HTML.

    Accept: GET[]
    GET: If HTML, show password_reset_done_page. If JSON, http_method_not_allowed.
    Return: If HTML, password_reset_done_page.
    """
    permission_classes = (AllowAny,)

    # Parameters for django.contrib.auth.views
    template_name = 'accounts/password_reset_done.html'
    # extra_context=None

    def get(self, request):
        # If password_reset from HTML, show_password_reset_page
        if request.accepted_renderer.format == 'html':
            return self.show_password_reset_done_page()
        # If password_reset from JSON, deny.
        return self.http_method_not_allowed(request)

    def show_password_reset_done_page(self):
        self.request._request.GET = self.request.GET
        return password_reset_done(self.request._request, template_name=self.template_name)


class PasswordResetConfirmView(GenericAPIView):
    """
    Confirms the changed password of user account.
    Password reset e-mail link is confirmed, therefore this resets the user's password.

    Accept: GET[], POST[new_password1, new_password2], Django URL argument[token, uid]
    GET: If HTML, show password_reset_page. If JSON, http_method_not_allowed.
    POST: If HTML, confirm_reset_password against django.contrib.auth.views.
        If JSON, confirm_reset_password against PasswordResetConfirmSerializer.
    Return: If HTML, password_reset_confirm_page. If JSON, success/fail message.
    """
    permission_classes = (AllowAny,)
    serializer_class = PasswordResetConfirmSerializer

    # Parameters for django.contrib.auth.views
    uidb64 = None
    token = None
    template_name = 'accounts/password_reset_confirm.html'
    # token_generator=default_token_generator
    # set_password_form=SetPasswordForm
    post_reset_redirect = reverse_lazy('accounts:password_reset_complete')
    # extra_context=None

    def get(self, request, **kwargs):
        self.uidb64 = kwargs.pop('uidb64')
        self.token = kwargs.pop('token')
        # If password_reset_confirm from HTML, show_password_reset_confirm_page
        if request.accepted_renderer.format == 'html':
            return self.show_password_reset_confirm_page()
        # If password_reset_confirm from JSON, deny.
        return self.http_method_not_allowed(request)

    def post(self, request, **kwargs):
        self.uidb64 = kwargs.pop('uidb64')
        self.token = kwargs.pop('token')
        # If password_reset_confirm from HTML, confirm_reset_password_from_html
        if request.accepted_renderer.format == 'html':
            return self.confirm_reset_password_from_html()
        return self.confirm_reset_password_from_json()

    def show_password_reset_confirm_page(self):
        self.request._request.GET = self.request.GET
        return password_reset_confirm(self.request._request, uidb64=self.uidb64,
                                      token=self.token,
                                      template_name=self.template_name,
                                      post_reset_redirect=self.post_reset_redirect)

    def confirm_reset_password_from_html(self):
        self.request._request.POST = self.request.POST
        return password_reset_confirm(self.request._request, uidb64=self.uidb64,
                                      token=self.token,
                                      template_name=self.template_name,
                                      post_reset_redirect=self.post_reset_redirect)

    def confirm_reset_password_from_json(self):
        serializer = self.get_serializer(data=self.request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response({"success": "Password has been reset with the new password."}, status=status.HTTP_200_OK)


class PasswordResetCompleteView(GenericAPIView):
    """
    Shows the password_reset_confirm_page only for HTML.

    Accept: GET[]
    GET: If HTML, show password_reset_confirm_page. If JSON, http_method_not_allowed.
    Return: If HTML, password_reset_confirm_page.
    """
    permission_classes = (AllowAny,)

    # Parameters for django.contrib.auth.views
    template_name = 'accounts/password_reset_complete.html'
    # extra_context=None

    def get(self, request):
        # If password_reset from HTML, show_password_reset_page
        if request.accepted_renderer.format == 'html':
            return self.show_password_reset_complete_page()
        # If password_reset from JSON, deny.
        return self.http_method_not_allowed(request)

    def show_password_reset_complete_page(self):
        self.request._request.GET = self.request.GET
        return password_reset_complete(self.request._request, template_name=self.template_name)

