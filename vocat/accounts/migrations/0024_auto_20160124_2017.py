# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-01-24 11:17
from __future__ import unicode_literals

import django.core.validators
from django.db import migrations, models
import re


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0023_auto_20160124_2006'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='name',
            field=models.CharField(help_text="Required. 70 characters or fewer. Characters, digits and hyphens('-') only.", max_length=70, validators=[django.core.validators.RegexValidator(re.compile("^[\\w\\s.'-]+$", 32), "Enter a valid username. This value may contain only letters, numbers, hyphen(-), apostrophe(')and single spaces.")], verbose_name='name'),
        ),
    ]
