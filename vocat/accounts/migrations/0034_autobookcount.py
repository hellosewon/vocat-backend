# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-02-01 23:50
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0033_auto_20160202_0644'),
    ]

    operations = [
        migrations.CreateModel(
            name='AutoBookCount',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('count', models.IntegerField(default=0)),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='auto_book_count', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
