# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-01-23 08:09
from __future__ import unicode_literals

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0003_userprofile'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='name',
            field=models.CharField(help_text="Required. 70 characters or fewer. Characters, digits and -/ /'/ only.", max_length=70, validators=[django.core.validators.RegexValidator("^[^\\W_]+([ \\-'\u2027][^\\W_]+)*?\\Z", 'Enter a valid name. This value may contain only letters, numbers, hyphen(-), apostrophe and single spaces.')], verbose_name='name'),
        ),
    ]
