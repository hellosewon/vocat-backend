# -- coding: utf-8 --
from __future__ import unicode_literals

import re
import uuid
from django.db import models
from django.db.models.signals import post_save
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.core import validators
from django.core.mail import send_mail
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone

from common.db_choices import SEX_CHOICES, LANGUAGE_CHOICES, DEVICE_TYPES


name_regex = re.compile(r"^[\w\s.'-]+$", re.U)


class UserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, email, name, password, **extra_fields):
        """
        Creates and saves a User with the given username, email and password.
        """
        if not email:
            raise ValueError('The given email must be set')
        if not name:
            raise ValueError('The given name must be set')
        email = self.normalize_email(email)
        name = self.normalize_name(name)
        user = self.model(email=email, name=name, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, name, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, name, password, **extra_fields)

    def create_superuser(self, email, name, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, name, password, **extra_fields)

    @staticmethod
    def normalize_name(name):
        name = name or ''
        name = " ".join(name.split())
        return name


class User(AbstractBaseUser, PermissionsMixin):
    """
    An abstract base class implementing a fully featured User model with
    admin-compliant permissions.

    Username and password are required. Other fields are optional.
    """
    email = models.EmailField(
        _('email address'),
        max_length=255,
        unique=True,
        help_text=_('Required. Valid email address.'),
        error_messages={
            'unique': _("A user with that email already exists."),
        },
    )
    name = models.CharField(
        _('name'),
        max_length=70,
        help_text=_("Required. 70 characters or fewer. Characters, numbers "
                    "hyphens(-), apostrophes(') dots(.) and single spaces only."),
        validators=[
            validators.RegexValidator(
                name_regex,
                _("Enter a valid username. This value may contain only "
                  "characters, numbers, hyphens(-), apostrophes(') "
                  "dots(.) and single spaces."),
            ),
        ],
    )
    is_staff = models.BooleanField(
        _('staff status'),
        default=False,
        help_text=_('Designates whether the user can log into this admin site.'),
    )
    is_active = models.BooleanField(
        _('active'),
        default=True,
        help_text=_(
            'Designates whether this user should be treated as active. '
            'Unselect this instead of deleting accounts.'
        ),
    )
    date_joined = models.DateTimeField(
            _('date joined'), default=timezone.now)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['name']

    def get_full_name(self):
        return self.name

    def get_short_name(self):
        return self.name

    def email_user(self, subject, message, from_email=None, **kwargs):
        """
        Sends an email to this User.
        """
        send_mail(subject, message, from_email, [self.email], **kwargs)


class UserProfile(models.Model):
    user = models.OneToOneField(User, related_name='profile', on_delete=models.CASCADE)
    sex = models.CharField(max_length=1, choices=SEX_CHOICES, blank=True)
    date_of_birth = models.DateField(null=True, blank=True)
    language_for = models.CharField(max_length=2, choices=LANGUAGE_CHOICES, default='EN')
    language_by = models.CharField(max_length=2, choices=LANGUAGE_CHOICES, default='KO')


class Device(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, related_name='devices', on_delete=models.CASCADE)
    type = models.IntegerField(choices=DEVICE_TYPES, default=0)


class AutoBookCount(models.Model):
    user = models.OneToOneField(User, related_name='auto_book_count', on_delete=models.CASCADE)
    count = models.IntegerField(default=0)


class AutoSectionCount(models.Model):
    user = models.OneToOneField(User, related_name='auto_section_count', on_delete=models.CASCADE)
    count = models.IntegerField(default=0)


class AutoVocabularyCount(models.Model):
    user = models.OneToOneField(User, related_name='auto_vocabulary_count', on_delete=models.CASCADE)
    count = models.IntegerField(default=0)


def create_user_stuff(sender, instance, created, **kwargs):
    """ Create a user profile when a new user account is created """
    if created:
        UserProfile.objects.create(user=instance)
        Device.objects.create(user=instance)
        AutoBookCount.objects.create(user=instance)
        AutoSectionCount.objects.create(user=instance)
        AutoVocabularyCount.objects.create(user=instance)

post_save.connect(create_user_stuff, sender=User)
