from rest_framework import generics, viewsets
from rest_framework.response import Response
from accounts.models import User
from testapp.models import *
from api.serializers import *

from rest_framework.views import APIView
from rest_framework.renderers import JSONRenderer, TemplateHTMLRenderer


class UserMixin(object):
    """
    Mix in for User
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer


class UserList(UserMixin, generics.ListAPIView):
    pass


class UserDetail(UserMixin, generics.RetrieveAPIView):
    pass


class WordBookMixin(object):
    queryset = Book.objects.all()
    serializer_class = WordBookSerializer


class WordBookList(WordBookMixin, generics.ListCreateAPIView):
    pass


class WordBookDetail(WordBookMixin, generics.RetrieveUpdateDestroyAPIView):
    pass


class BookSectionMixin(object):
    queryset = Section.objects.all()
    serializer_class = BookSectionSerializer


class BookSectionList(BookSectionMixin, generics.ListCreateAPIView):
    pass


class BookSectionDetail(BookSectionMixin, generics.RetrieveUpdateDestroyAPIView):
    pass


class VocabularyMixin(object):
    queryset = Vocabulary.objects.all()
    serializer_class = VocabularySerializer


class VocabularyList(VocabularyMixin, generics.ListCreateAPIView):
    pass


class VocabularyDetail(VocabularyMixin, generics.RetrieveUpdateDestroyAPIView):
    pass


# class TaskMixin(object):
#     queryset = Task.objects.all()
#     serializer_class = TaskSerializer
#     # permission_classes = (IsAdminUser,)
#     # paginate_by = 100


# class TaskList(TaskMixin, generics.ListCreateAPIView):
#     pass


# class TaskViewSet(viewsets.ModelViewSet):
#     queryset = Task.objects.all()
#     serializer_class = TaskSerializer
#     template_name = 'test.html'
#
#     def list(self, request, *args, **kwargs):
#         response = super(TaskViewSet, self).list(request, *args, **kwargs)
#         if request.accepted_renderer.format == 'html':
#             return Response({'data': response.data}, template_name=self.template_name)
#         return response

#
# class TaskList(TaskMixin, generics.ListCreateAPIView):
#     template_name = 'test.html'
#
#     def get(self, request, *args, **kwargs):
#         if request.accepted_renderer.format == 'html':
#             print 'hi', self.allowed_methods
#             data = {'data': self.get_queryset()}
#             return Response(data, template_name=self.template_name)
#         serializer = self.get_serializer(self.get_queryset(), many=True)
#         data = serializer.data
#         return Response(data)

#
# class TaskDetail(TaskMixin, generics.RetrieveUpdateDestroyAPIView):
#     """
#     Allow: GET, POST, PATCH, DELETE
#     GET: Return a specific task
#     POST: Update a new task
#     PATCH: Create/Update a specific task
#     DELETE: Delete a specific task
#     """
#     pass
#
#
