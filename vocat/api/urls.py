from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^users/$', UserList.as_view(), name='user-list'),
    url(r'^users/(?P<pk>[0-9]+)$', UserDetail.as_view(), name='user-detail'),

    url(r'^wordbooks/$', WordBookList.as_view(), name='wordbook-list'),
    url(r'^wordbooks/(?P<pk>[0-9]+)$', WordBookDetail.as_view(), name='wordbook-detail'),
    url(r'^booksections/$', BookSectionList.as_view(), name='booksection-list'),
    url(r'^booksections/(?P<pk>[0-9]+)$', BookSectionDetail.as_view(), name='booksection-detail'),
    url(r'^vocabularies/$', VocabularyList.as_view(), name='vocabulary-list'),
    url(r'^vocabularies/(?P<pk>[0-9]+)$', VocabularyDetail.as_view(), name='vocabulary-detail'),

    # url(r'^tasks/$', TaskList.as_view(), name='task_list'),
    # url(r'^tasks/(?P<pk>[0-9]+)$', TaskDetail.as_view(), name='task_detail'),
]

