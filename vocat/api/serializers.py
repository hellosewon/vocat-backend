from rest_framework import serializers

from testapp.models import *


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('id', 'email', 'name')


class VocabularySerializer(serializers.ModelSerializer):
    section = serializers.SlugRelatedField(
        read_only=True,
        slug_field='title'
    )

    class Meta:
        model = Vocabulary


class BookSectionSerializer(serializers.ModelSerializer):
    book = serializers.SlugRelatedField(
        read_only=True,
        slug_field='title'
    )
    vocabularies = VocabularySerializer(many=True, read_only=True)

    class Meta:
        model = Section


class WordBookSerializer(serializers.ModelSerializer):
    sections = BookSectionSerializer(many=True, read_only=True)

    class Meta:
        model = Book



