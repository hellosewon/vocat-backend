from __future__ import unicode_literals

from django.db import models
from django.db.models.signals import post_save
from django.utils import timezone

from common.db_choices import LANGUAGE_CHOICES, SECTION_TYPES

from accounts.models import (
    User, Device, AutoBookCount, AutoSectionCount, AutoVocabularyCount
)


# null=True --> Can be none. Sets NULL in DB Column.
# null=False --> Cannot be none.
# blank=True --> Field not required.
# blank=False --> Field required.
# Blank values for DateTimeField and ForeignKeyField will be stored as NULL in DB.
# CharFields and TextFields, which in Django are never saved as NULL.
# Blank values for CharField and TextFields are stored in the DB as an empty string ('').
# Default in Django: null=False, blank=False

# WordBook can exist on its own even if the creator's account had been deleted.
# WordBook cannot exit on its own if the owner's account had been deleted.
# (WordBook in Market is owner=null and is simply a duplicate of the original wordbook.)


class Book(models.Model):
    id = models.CharField(primary_key=True, max_length=255)
    date_created = models.DateTimeField()
    date_updated = models.DateTimeField()

    language_for = models.CharField(max_length=2, choices=LANGUAGE_CHOICES, default='EN')
    language_by = models.CharField(max_length=2, choices=LANGUAGE_CHOICES, default='KO')
    # copy_of field is for identifying the origin of the book.
    # Even if the original book was deleted, the book should still remain. So, null=True.
    # The copy_of field is not required in initial creation. So, blank=True.
    copy_of = models.ForeignKey('self', null=True, blank=True, related_name='copied_books', on_delete=models.SET_NULL)
    # Even if the creator's account has been deleted, the book should still remain. So, null=True.
    # But creator field is required in initial creation. So, blank=False.
    creator = models.ForeignKey(User, null=True, related_name='created_books', on_delete=models.SET_NULL)
    # Owner cannot be nonexistent. So, null=False.
    # The owner field is required to be filled in initial creation. So, blank=False.
    owner = models.ForeignKey(User, related_name='owned_books', on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    # The description field cannot be null since it is a textfield, but must be empty string (''). So, null=False.
    # The description field is not required. So, blank=True.
    description = models.TextField(blank=True)
    is_public = models.BooleanField(default=False)
    # Both liked_by and disliked_by fields can be null, but it is set to null=False
    # because null=True is not required in ManyToManyField, just blank=True.
    # Both liked_by and disliked_by fields are not required. So, blank=True.
    likes = models.ManyToManyField(User, blank=True, related_name='liked_books')
    dislikes = models.ManyToManyField(User, blank=True, related_name='disliked_books')

    def save(self, *args, **kwargs):
        # If posted from browser
        if self.id == '0':
            owner = self.owner
            device_obj, created = Device.objects.get_or_create(user=owner, type=0)
            # Get and update user's book count
            count_obj, created = AutoBookCount.objects.get_or_create(user=owner)
            count = count_obj.count + 1
            count_obj.count = count
            count_obj.save()
            self.id = str(device_obj.id) + '-' + str(count)
        else:
            book_id = self.id.split('-')
            if len(book_id) != 2:
                raise ValueError('Invalid book id')
            if not Device.objects.filter(id=int(book_id[0])).exists():
                raise ValueError('Device non existent')
        now = timezone.now()
        if self.date_created is None:
            self.date_created = now
        if self.date_updated is None:
            self.date_updated = now
        super(Book, self).save(*args, **kwargs)

    class Meta:
        ordering = ['owner', 'date_created']

    def __unicode__(self):
        return self.title

    def count_section(self):
        return self.sections.count()

    def count_vocab(self):
        return Vocabulary.objects.filter(section__book__exact=self).count()

    def count_likes(self):
        return self.likes.count()

    def count_dislikes(self):
        return self.dislikes.count()


# Section cannot exist on its own. It depends on the Book.
# There are two Types of the section (for now). Blank(for representing book)
class Section(models.Model):
    id = models.CharField(primary_key=True, max_length=255)
    date_updated = models.DateTimeField()

    book = models.ForeignKey(Book, related_name='sections', on_delete=models.CASCADE)
    type = models.IntegerField(choices=SECTION_TYPES, default=1)
    sort = models.IntegerField()
    title = models.CharField(max_length=255)
    # The description field cannot be null since it is a textfield, but must be empty string (''). So, null=False.
    # The description field is not required. So, blank=True.
    description = models.TextField(blank=True)

    def save(self, *args, **kwargs):
        # If posted from browser
        if self.id == '0':
            owner = self.book.owner
            device_obj, created = Device.objects.get_or_create(user=owner, type=0)
            # Get and update user's section count
            count_obj, created = AutoSectionCount.objects.get_or_create(user=owner)
            count = count_obj.count + 1
            count_obj.count = count
            count_obj.save()
            self.id = str(device_obj.id) + '-' + str(count)
        else:
            section_id = self.id.split('-')
            if len(section_id) != 2:
                raise ValueError('Invalid section id')
            if not Device.objects.filter(id=int(section_id[0])).exists():
                raise ValueError('Device non existent')
        now = timezone.now()
        if self.date_updated is None:
            self.date_updated = now
        super(Section, self).save(*args, **kwargs)

    class Meta:
        # unique_together = ('order', 'book')
        ordering = ['book', 'sort']

    def __unicode__(self):
        return self.title

    def get_owner(self):
        return self.book.owner

    def count_vocab(self):
        return self.vocabularies.count()


# Vocabulary cannot exist on its own. It depends on Section < Book.
class Vocabulary(models.Model):
    id = models.CharField(primary_key=True, max_length=255)
    date_updated = models.DateTimeField()

    section = models.ForeignKey(Section, related_name='vocabularies', on_delete=models.CASCADE)
    sort = models.IntegerField()
    name = models.CharField(max_length=255)
    meaning = models.TextField(blank=True)

    def save(self, *args, **kwargs):
        # If posted from browser
        if self.id == '0':
            owner = self.section.book.owner
            device_obj, created = Device.objects.get_or_create(user=owner, type=0)
            # Get and update user's vocabulary count
            count_obj, created = AutoVocabularyCount.objects.get_or_create(user=owner)
            count = count_obj.count + 1
            count_obj.count = count
            count_obj.save()
            self.id = str(device_obj.id) + '-' + str(count)
        else:
            vocabulary_id = self.id.split('-')
            if len(vocabulary_id) != 2:
                raise ValueError('Invalid vocabulary id')
            if not Device.objects.filter(id=int(vocabulary_id[0])).exists():
                raise ValueError('Device non existent')
        now = timezone.now()
        if self.date_updated is None:
            self.date_updated = now
        super(Vocabulary, self).save(*args, **kwargs)

    class Meta:
        # unique_together = ('sort', 'section')
        ordering = ['section', 'sort']
        verbose_name_plural = "vocabularies"

    def __unicode__(self):
        return self.name

    def get_owner(self):
        return self.section.book.owner

    def get_book(self):
        return self.section.book


def create_default_section(sender, instance, created, **kwargs):
    """ Create a default section when a new book is created """
    # Automatically create section only when the book is created via web (not app).
    book_id = instance.id.split('-')
    device_obj, d_created = Device.objects.get_or_create(user=instance.owner, type=0)
    if created and book_id[0] == str(device_obj.id):
        Section.objects.create(id='0', book=instance, type=0, sort=0, title='default')


post_save.connect(create_default_section, sender=Book)




# Knowhow can exist on its own, independent of vocab, section and book.
# class Knowhow(models.Model):
#     #id = models.CharField(primary_key=True, max_length=255)
#     date_created = models.DateTimeField(null=True, blank=False)
#     date_updated = models.DateTimeField(null=True, blank=False)
#
#     language_for = models.CharField(max_length=2, choices=LANGUAGE_CHOICES, default='EN')
#     language_by = models.CharField(max_length=2, choices=LANGUAGE_CHOICES, default='KO')
#     # Even if the creator's account has been deleted, the knowhow should still remain. So, null=True.
#     # But creator field is required in initial creation. So, blank=False.
#     creator = models.ForeignKey(User, null=True, related_name='created_knowhows', on_delete=models.SET_NULL)
#     # Knowhows can be owned by many users. Owning knowhow means keeping the knowhow in 'My Knowhow'.
#     # The owner field can be null, but null=False since null=True is not required in ManyToManyField, just blank=True.
#     # The owner field is not required. So, blank=True.
#     owners = models.ManyToManyField(User, blank=True, related_name='owned_knowhows')  # Subscribe
#     # vocabulary = models.CharField(max_length=255)
#     # knowhow content is required and cannot be null (since it is a text field).
#     content = models.TextField()
#     # The post_from field is used to track in what vocabulary<section<book it was posted
#     # and show that post_from information to other users.
#     # Even if the vocabulary has been deleted, the knowhow should still remain. So, null=True.
#     # The field post_from is not required. So, blank=True.
#     post_from = models.ForeignKey(Vocabulary, null=True, blank=True, related_name='posted_knowhows', on_delete=models.SET_NULL)
#     is_public = models.BooleanField(default=True)
#     # Both liked_by and disliked_by fields can be null, but it is set to null=False
#     # because null=True is not required in ManyToManyField, just blank=True.
#     # Both liked_by and disliked_by fields are not required. So, blank=True.
#     likes = models.ManyToManyField(User, blank=True, related_name='liked_knowhows')
#     dislikes = models.ManyToManyField(User, blank=True, related_name='disliked_knowhows')
#
#     def save(self, *args, **kwargs):
#         knowhow_id = self.id.split('-')
#         if len(knowhow_id) != 2:
#             raise ValueError('Invalid knowhow id')
#
#         if not Device.objects.filter(id=int(knowhow_id[0])).exists():
#             raise ValueError('Device non existent')
#
#         now = timezone.now()
#         if self.date_created is None:
#             self.date_created = now
#         if self.date_updated is None:
#             self.date_updated = now
#         super(Knowhow, self).save(*args, **kwargs)


