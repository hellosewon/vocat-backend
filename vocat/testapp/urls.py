from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^$', Home.as_view(), name='home'),

    url(r'^admin/book_upload', BookUploadView.as_view(), name='book-upload'),

    url(r'^books/$', BookList.as_view(), name='book-list'),
    url(r'^books/(?P<pk>[0-9]+-[0-9]+)/$', BookDetail.as_view(), name='book-detail'),

    url(r'^book-market/$', BookMarketList.as_view(), name='book-market'),
    url(r'^book-market/(?P<pk>[0-9]+-[0-9]+)/$', BookMarketDetail.as_view(), name='book-market-detail'),

    url(r'^sections/$', SectionList.as_view(), name='section-list'),
    url(r'^sections/(?P<pk>[0-9]+-[0-9]+)/$', SectionDetail.as_view(), name='section-detail'),
    url(r'^vocabularies/$', VocabularyList.as_view(), name='vocabulary-list'),
    url(r'^vocabularies/(?P<pk>[0-9]+-[0-9]+)/$', VocabularyDetail.as_view(), name='vocabulary-detail'),

    url(r'^sync/$', SyncList.as_view(), name='sync'),
]
