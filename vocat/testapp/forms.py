from django import forms
from .models import Book


class BookCreationForm(forms.ModelForm):

    class Meta:
        model = Book
        fields = ('language_for', 'language_by', 'title', 'description', 'is_public')
