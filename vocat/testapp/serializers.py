from rest_framework import serializers
from accounts.serializers import UserDetailSerializer
from .models import Book, Section, Vocabulary


# Showing wordbooks I own
class BookSerializer(serializers.ModelSerializer):
    section_count = serializers.IntegerField(source='sections.count', read_only=True)
    vocab_count = serializers.IntegerField(source='count_vocab', read_only=True)
    #creator = UserDetailSerializer(read_only=True)
    #owner = UserDetailSerializer(read_only=True)

    class Meta:
        model = Book


# Showing sections I own
class SectionSerializer(serializers.ModelSerializer):
    vocab_count = serializers.IntegerField(source='count_vocab', read_only=True)

    class Meta:
        model = Section


# Showing vocabularies I own
class VocabularySerializer(serializers.ModelSerializer):

    class Meta:
        model = Vocabulary



class SectionDownloadSerializer(SectionSerializer):
    vocabularies = VocabularySerializer(many=True)


class BookDownloadSerializer(BookSerializer):
    sections = SectionDownloadSerializer(many=True)
    # owner = UserDetailSerializer(read_only=True)