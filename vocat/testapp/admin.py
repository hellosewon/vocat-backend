from django.contrib import admin

from .models import Book, Section, Vocabulary#, Knowhow


class BookAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'count_section', 'count_vocab', 'is_public', 'copy_of', 'creator', 'owner', 'count_likes', 'count_dislikes')


class SectionAdmin(admin.ModelAdmin):
    list_display = ('id', 'get_owner', 'book', 'type', 'sort', 'title', 'count_vocab', 'description')


class VocabularyAdmin(admin.ModelAdmin):
    list_display = ('id', 'get_owner', 'get_book', 'section', 'sort', 'name')


class KnowhowAdmin(admin.ModelAdmin):
    list_display = ('creator',)


admin.site.register(Book,  BookAdmin)
admin.site.register(Section, SectionAdmin)
admin.site.register(Vocabulary, VocabularyAdmin)
#admin.site.register(Knowhow, KnowhowAdmin)
