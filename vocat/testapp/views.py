from django.views.generic import View
from django.template.response import TemplateResponse
from rest_framework.parsers import FileUploadParser
from rest_framework import generics, filters
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, IsAdminUser, BasePermission, SAFE_METHODS
from .serializers import *
from .models import *
from .forms import BookCreationForm

import csv, codecs


class Home(View):
    template_anonymous = 'testapp/home.html'
    template_mainapp = 'testapp/home_mainapp.html'

    def get(self, request):
        if request.user.is_authenticated():
            return self.show_home_mainapp()
        return self.show_home_anonymous()

    def show_home_anonymous(self):
        data = User.objects.all().count()
        context = {
        #     'title': _('Profile'),
            'data': data,
        }
        return TemplateResponse(self.request, self.template_anonymous, context)

    def show_home_mainapp(self):
        context = {
            # 'title': _('Profile'),
            # 'data': data,
        }
        return TemplateResponse(self.request, self.template_mainapp, context)


class IsBookOwnerOrPublicReadOnly(BasePermission):
    def has_object_permission(self, request, view, obj):
        # If request is GET, HEAD or OPTION
        if request.method in SAFE_METHODS:
            # you can GET any public book or book of your own.
            return obj.is_public or obj.owner == request.user
        # If request is PUT, PATCH, DELETE, you must be the owner.
        return obj.owner == request.user


class IsSectionOwnerOrPublicReadOnly(BasePermission):
    def has_object_permission(self, request, view, obj):
        # If request is GET, HEAD or OPTION
        if request.method in SAFE_METHODS:
            # you can GET any public book or book of your own.
            return obj.book.is_public or obj.book.owner == request.user
        # If request is PUT, PATCH, DELETE, you must be the owner.
        return obj.book.owner == request.user


class IsVocabularyOwnerOrPublicReadOnly(BasePermission):
    def has_object_permission(self, request, view, obj):
        # If request is GET, HEAD or OPTION
        if request.method in SAFE_METHODS:
            # you can GET any public book or book of your own.
            return obj.section.book.is_public or obj.section.book.owner == request.user
        # If request is PUT, PATCH, DELETE, you must be the owner.
        return obj.section.book.owner == request.user


class BookUploadView(generics.GenericAPIView):
    permission_classes = (IsAdminUser,)
    template_name = 'testapp/book_upload.html'

    book_creation_form = BookCreationForm

    def get(self, request):
        context = {'form': self.book_creation_form}
        return TemplateResponse(self.request, self.template_name, context)

    def post(self, request):
        if not request.FILES:
            return Response(status=400)
        file_obj = request.FILES['file']
        book = Book.objects.create(id='0', owner=request.user, creator=request.user,
                                   is_public=request.POST.get('is_public', False),
                                   language_for=request.POST['language_for'], language_by=request.POST['language_by'],
                                   title=request.POST['title'], description=request.POST['description'])

        reader = csv.DictReader(file_obj)
        s_sort, v_sort = 1, 1
        for row in reader:
            print 'Creating - ', row['vocabulary']
            try:
                section = Section.objects.get(book__exact=book, title=row['section'], sort=s_sort-1)
            except Section.DoesNotExist:
                if row['section'] is None or row['section'] == '':
                    section = Section.objects.get(book__exact=book, type=0)
                else:
                    section = Section.objects.create(id='0', book=book, sort=s_sort, title=row['section'])
                s_sort += 1
                v_sort = 1
            if row['vocabulary'] is not None and row['vocabulary'] != '':
                Vocabulary.objects.create(id='0', section=section, sort=v_sort, name=row['vocabulary'],
                                          meaning=row['meaning'])
                v_sort += 1
        return Response(status=200)


class BookList(generics.ListCreateAPIView):
    """
    GET: Return a list of user-owned books (if authenticated).
    POST: Create & return a user-owned book (if authenticated).
    """
    serializer_class = BookSerializer
    permission_classes = (IsAuthenticated,)
    template_name = 'testapp/books.html'
    # pagination_class = LargeResultsSetPagination

    def get_queryset(self):
        user = self.request.user
        books = user.owned_books.all()
        return books

    def get(self, request, *args, **kwargs):
        serializer = self.get_serializer(self.get_queryset(), many=True)
        data = serializer.data
        if request.accepted_renderer.format == 'html':
            data = {'data': data}
            return Response(data, template_name=self.template_name)

        return Response(data)


class BookDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    GET: Return Book detail (if authenticated).
    PUT: Create & return Book detail (if authenticated).
    PATCH: Update Book detail (if authenticated and owner).
    DELETE: Delete Book (if authenticated and owner).
    """
    queryset = Book.objects.all()
    serializer_class = BookSerializer
    permission_classes = (IsAuthenticated, IsBookOwnerOrPublicReadOnly)


class SectionList(generics.ListCreateAPIView):
    """
    GET: Return a list of user-owned sections (if authenticated).
    POST: Create & return a user-owned section (if authenticated).
    """
    serializer_class = SectionSerializer
    permission_classes = (IsAuthenticated,)
    # template_name = 'testapp/books.html'
    # pagination_class = LargeResultsSetPagination

    def get_queryset(self):
        user = self.request.user
        sections = Section.objects.filter(book__owner__exact=user)
        return sections

    def get(self, request, *args, **kwargs):
        serializer = self.get_serializer(self.get_queryset(), many=True)
        data = serializer.data
        # if request.accepted_renderer.format == 'html':
        #     data = {'data': data}
        #     return Response(data, template_name=self.template_name)

        return Response(data)


class SectionDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    GET: Return Section detail (if authenticated).
    PUT: Create & return Section detail (if authenticated).
    PATCH: Update Section detail (if authenticated and owner).
    DELETE: Delete Section (if authenticated and owner).
    """
    queryset = Section.objects.all()
    serializer_class = SectionSerializer
    permission_classes = (IsAuthenticated, IsSectionOwnerOrPublicReadOnly)


class VocabularyList(generics.ListCreateAPIView):
    """
    GET: Return a list of user-owned vocabularies (if authenticated).
    POST: Create & return a user-owned vocabularies (if authenticated).
    """
    serializer_class = VocabularySerializer
    permission_classes = (IsAuthenticated,)
    # template_name = 'testapp/books.html'
    # pagination_class = LargeResultsSetPagination

    def get_queryset(self):
        user = self.request.user
        vocabularies = Vocabulary.objects.filter(section__book__owner__exact=user)
        return vocabularies

    def get(self, request, *args, **kwargs):
        serializer = self.get_serializer(self.get_queryset(), many=True)
        data = serializer.data
        # if request.accepted_renderer.format == 'html':
        #     data = {'data': data}
        #     return Response(data, template_name=self.template_name)

        return Response(data)


class VocabularyDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    GET: Return Vocabulary detail (if authenticated).
    PUT: Create & return Vocabulary detail (if authenticated).
    PATCH: Update Vocabulary detail (if authenticated and owner).
    DELETE: Delete Vocabulary (if authenticated and owner).
    """
    queryset = Vocabulary.objects.all()
    serializer_class = VocabularySerializer
    permission_classes = (IsAuthenticated, IsVocabularyOwnerOrPublicReadOnly)


class BookMarketList(generics.ListCreateAPIView):
    """
    GET: Return a list of public books.
    POST: Create & return a user-owned public book.
    """
    serializer_class = BookSerializer
    permission_classes = (IsAuthenticated,)
    template_name = 'testapp/book_market.html'
    # pagination_class = LargeResultsSetPagination

    def get_queryset(self):
        books = Book.objects.filter(is_public=True)
        return books

    def get(self, request, *args, **kwargs):
        serializer = self.get_serializer(self.get_queryset(), many=True)
        data = serializer.data
        if request.accepted_renderer.format == 'html':
            data = {'data': data}
            return Response(data, template_name=self.template_name)
        return Response(data)


class BookMarketDetail(generics.RetrieveAPIView):
    queryset = Book.objects.filter(is_public=True)
    serializer_class = BookDownloadSerializer
    permission_classes = (IsAuthenticated,)


class SyncList(generics.ListCreateAPIView):
    serializer_class = BookDownloadSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        user = self.request.user
        books = user.owned_books.all()
        return books
