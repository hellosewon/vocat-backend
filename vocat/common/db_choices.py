# -- coding: utf-8 --
from __future__ import unicode_literals
SEX_CHOICES = (
    ('M', 'Male'),
    ('F', 'Female'),
)

LANGUAGE_CHOICES = (
    ('KO', 'Korean'),
    ('EN', 'English'),
)

SECTION_TYPES = (
    (0, 'Default'),
    (1, 'Normal'),
)

DEVICE_TYPES = (
    (0, 'Browser'),
    (1, 'Android'),
    (2, 'iOS'),
)
