from django.conf import settings
from storages.backends.s3boto import S3BotoStorage


class StaticS3BotoStorage(S3BotoStorage):
    """
    Custom S3 storage backends to store files in different buckets/subfolders.
    """
    bucket_name = getattr(settings, 'AWS_S3_STATIC_BUCKET_NAME', None)
    custom_domain = getattr(settings, 'AWS_S3_STATIC_DOMAIN', None)
    # location = 'static'


class MediaS3BotoStorage(S3BotoStorage):
    """
    Custom S3 storage backends to store files in different buckets/subfolders.
    """
    bucket_name = getattr(settings, 'AWS_S3_MEDIA_BUCKET_NAME', None)
    custom_domain = getattr(settings, 'AWS_S3_MEDIA_DOMAIN', None)
    # location = 'media'


# For different subfolders
# StaticRootS3BotoStorage = lambda: S3BotoStorage(location='static')
# MediaRootS3BotoStorage = lambda: S3BotoStorage(location='media')
